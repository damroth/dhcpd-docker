# dhcpd-docker


## What's this?

Very simple dhcpd server based on the latest apline linux for testing purposes.

## Dependencies
You'll need to install docker, docker-compose and make.

## How to run it?
`make run`

## How to configure it?
take a look at `dhcpd.conf` file it's all you need.